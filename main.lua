-- *************************************
-- *                                   *
-- *            Map editor             *
-- *                                   *
-- *************************************
local g3d = require "code/libraries/g3d"

-- Includes 
require "code/init"
require "code/classes/map"

camera = {
	x=-40,
	y=15,
	z=0,
}

-- *************************************
-- *           Load callback           *
-- *************************************
function love.load(arg)

	-- Init camera 
	g3d.camera.fov = math.rad(25)
	g3d.camera.updateViewMatrix()
	g3d.camera.updateProjectionMatrix()

	-- Init map
	lahan = map:new{lib = g3d}	 
	lahan:init("test.obj", "test.png")
	
end

-- *************************************
-- *          Update callback          *
-- *************************************
function love.update(dt)
	
	-- Position and orient camera
	g3d.camera.lookInDirection(camera.x, camera.z, camera.y, math.rad(0), math.rad(-20))
	
end

-- *************************************
-- *           Draw callback           *
-- *************************************
function love.draw()
    
	-- Render weltall
	lahan.model:draw()
	
	-- Print cam data	
	love.graphics.print("Camera X: "..tostring(camera.x), 10, 10)
	love.graphics.print("Camera Y: "..tostring(camera.y), 10, 25)
	love.graphics.print("Camera Z: "..tostring(camera.z), 10, 40)
	
end


-- *************************************
-- *         Control callbacks         *
-- *************************************
-- Keypress function
function love.keypressed(key,scancode,isrepeat)
	
	if key == "up" then
		camera.x = camera.x + 0.5
	elseif key == "down" then
		camera.x = camera.x - 0.5	
	elseif key == "left" then
		camera.z = camera.z + 0.1
	elseif key == "right" then
		camera.z = camera.z - 0.1
	end
	
end

-- Keyreleased function
function love.keyreleased(key,scancode)

end