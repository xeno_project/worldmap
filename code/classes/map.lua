-- *************************************
-- *                                   *
-- *               Map                 *
-- *                             Class *
-- *************************************
map = {};function map:new(o)
	
    -- constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties
		o.name = "Lahan"
		
		-- Nested properties  
		o.model = {}
		o.position = {
			x=0,
			y=0,
			z=0
		}
		
		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *        map initialization        *
	-- *************************************	
	function self:init(fileName, textureName)
	
		-- Load model 
		self.model = self.lib.newModel(
			"assets/models/map/"..fileName, 
			"assets/models/map/"..textureName, 
			{0, 0, 0}, -- Position 
			{math.rad(90), math.rad(0), math.rad(0)}, -- Rotation 
			{20, 20, 20} -- Scale
		)
	
		return true
	
	end
	
	
	-- *************************************
	-- *         Map interactions          *
	-- *************************************	
	
	
	
	return o

end